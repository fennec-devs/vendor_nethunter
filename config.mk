#Copyright (C) 2020 Fennec-devs

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

VENDOR_PATH := vendor/NetHunter

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/etc,system/etc) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/bin,system/bin) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/xbin,system/xbin) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/sbin,system/sbin) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/app/NetHunterKeX/lib,system/app/NetHunterKeX/lib) \
    $(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/app/NetHunterTerminal/lib,system/app/NetHunterTerminal/lib)

PRODUCT_PACKAGES += \
    NetHunter \
    NetHunterTerminal \
    NetHunterKeX \
    NetHunterStore \
    NetHunterStorePrivilegedExtension
